MOVIE_LIST_URL='https://www.imdb.com/list/ls033800340/export?ref_=ttls_exp'
CSV_PATH=$HOME/.cache/movies.csv
DB_PATH=$HOME/.cache/movies.db

curl -o $CSV_PATH $MOVIE_LIST_URL

if [[ -f $DB_PATH ]]; then
    rm $DB_PATH
fi

sqlite3 $DB_PATH ".mode csv" ".import $CSV_PATH movies"
