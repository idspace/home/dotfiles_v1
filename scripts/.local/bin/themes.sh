#!/bin/sh

THEMES_DIRECTORY="${XDG_DATA_HOME}/themes"

source $HOME/.cache/wal/colors.sh

function alacritty_colors () {
  eval "cat <<EOF
$(<$THEMES_DIRECTORY/alacritty.theme)
EOF
" > ${XDG_CONFIG_HOME}/alacritty/alacritty.theme.toml
}

function nvim_colors () {
  eval "cat <<EOF
$(<$THEMES_DIRECTORY/nvim.theme)
EOF
" > ${XDG_CONFIG_HOME}/nvim/lua/custom/themes/wal.lua
}

function zathura_colors () {
  eval "cat <<EOF
$(<$THEMES_DIRECTORY/zathura.theme)
EOF
" > ${XDG_CONFIG_HOME}/zathura/zathura.theme
}

function zellij_colors () {
  eval "cat <<EOF
$(<$THEMES_DIRECTORY/zellij.theme)
EOF
" > ${XDG_CONFIG_HOME}/zellij/themes/iduoad.kdl
}

alacritty_colors
nvim_colors
zathura_colors
zellij_colors
