# Add scripts folder to PATH
export PATH="$PATH:$HOME/.gem/ruby/2.6.0/bin:$HOME/.local/bin:$HOME/.yarn/bin"

# Disable auto title (for tmuxp)
export DISABLE_AUTO_TITLE='true'

# Default programs
export BROWSER='firefox'
export EDITOR='nvim'
export FILE='vifm'
export READER="zathura"
export TERMINAL='alacritty'

##### XDG OVERRIDES #####
# XDG defaults
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_RUNTIME_DIR="/run/user/$(id -u)"
export XDG_STATE_HOME="$HOME/.local/state"

# set vagrant home
export VAGRANT_HOME="$XDG_DATA_HOME"/vagrant
export VAGRANT_ALIAS_FILE="$XDG_DATA_HOME"/vagrant/aliases

# set aws home
export AWS_SHARED_CREDENTIALS_FILE="$XDG_CONFIG_HOME"/aws/credentials
export AWS_CONFIG_FILE="$XDG_CONFIG_HOME"/aws/config

# set wget home
export WGETRC="$XDG_CONFIG_HOME/wgetrc"

# set lesshst directory
export LESSHISTFILE="$XDG_STATE_HOME"/less/history

# set vscode home
export VSCODE_EXTENSIONS="${XDG_DATA_HOME:-~/.local/share}/vscode-oss/extensions"

# set gnupg home
export GNUPGHOME="$XDG_DATA_HOME"/gnupg

# get rid of gem dir in $HOME
export GEM_HOME="$XDG_DATA_HOME"/gem
export GEM_SPEC_CACHE="$XDG_CACHE_HOME"/gem

# aspell Config and Data
export ASPELL_CONF="per-conf $XDG_CONFIG_HOME/aspell/aspell.conf; personal $XDG_CONFIG_HOME/aspell/en.pws; repl $XDG_CONFIG_HOME/aspell/en.prepl"

# avoid error.log in ~/.ncmpcpp
export ncmpcpp_directory='$HOME/.config/ncmpcpp'

# set Golang home
export GOPATH="$XDG_DATA_HOME"/go

# set elinks config home
export ELINKS_CONFDIR="$XDG_CONFIG_HOME"/elinks

# set OpenSSL ran file home
export RANDFILE="$XDG_STATE_HOME/openssl/.rnd"

# add dictionaries path
export STARDICT_DATA_DIR="$XDG_DATA_HOME/stardict"
export PASSWORD_STORE_DIR="$XDG_DATA_HOME"/pass

# rtv
export RTV_BROWSER='urlportal'

export ANSIBLE_HOME="$XDG_DATA_HOME"/ansible

# Change sqlite_history
export SQLITE_HISTORY=$XDG_STATE_HOME/sqlite/history

# Change Z directories
export _Z_DATA="$XDG_DATA_HOME/z"

# set azure config directory
export AZURE_CONFIG_DIR=$XDG_CONFIG_HOME/azure

# Change zhistory and compinit to use cache folder
export HISTFILE="$XDG_STATE_HOME"/zsh/history
compinit -d $XDG_CACHE_HOME/zsh/zcompdump-$ZSH_VERSION
