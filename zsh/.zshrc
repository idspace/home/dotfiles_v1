export ZSH_COMPDUMP=$XDG_CACHE_HOME/.zcompdump-${HOST}-${ZSH_VERSION}

export ZSH="/home/guru/.oh-my-zsh"

ZSH_THEME="aussiegeek"

plugins=(git fzf zsh-syntax-highlighting lxd-completion-zsh kubectl terraform aws)

source $ZSH/oh-my-zsh.sh

export LANG=en_US.UTF-8
export LC_CTYPE=en_US.UTF-8
export LC_ALL=en_US.UTF-8

eval "$(zoxide init zsh)"
eval "$(atuin init zsh --disable-up-arrow)"
