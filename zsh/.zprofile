# Start ssh agent

source .zshrc

eval `keychain --absolute --dir "${XDG_RUNTIME_DIR:/run/user/1000}"/keychain --eval --agents ssh id_rsa`

# Start graphical server on tty1 if not already running.
if [ "$(tty)" = "/dev/tty1" ] && ! pgrep -x Xorg >/dev/null;
then
    exec startx > $HOME/.local/share/xorg/startx.log 2>&1
fi
