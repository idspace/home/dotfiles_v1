# Packages
## Core packages
| package | description        |
| ---     | ---                |
| stow    | dotfile management |
| neovim  | text editor        |
| zsh | shell |
| tmux | multiplexer |
| vifm | file manager |
| htop | monitor |
| openssh | ssh |
| wget | downloader |
| zip | zip |
| unzip | unzip |
| unrar | unrar |
| rsync | file copier |

## System packages
| package        | description    |
| ---            | ---            |
| linux-firmware | linux firmware |
| ntfs-3g | ntfs drivers |
| exfat-utils | exfat drivers |
| lvm2 | lvm |
| alsa-utils | alsa utilities |
| alsa-plugins | alsa plugins |
| alsa-firmware | alsa firmware |
| pulseaudio | sound server |
| pulseaudio-equalizer | equalizer for PA |
| pulseaudio-alsa | alsa confs for PA |
| xf86-input-libinput | more drivers |
| xf86-video-intel | graphics drivers |
| mesa | opengl implementation |
| iw | wireless config |
| wpa_supplicant | key negotiation for WPA |
| dialog | dialog boxes |
| ifplugd | wired auto connection |

## Desktop packages
| package     | description |
| ---         | ---         |
| xorg-server | x server    |
| xorg-xinit | xinit |
| xorg-xbacklight | backlight |
| xclip | clipboard manager |
| arandr | xrander frontend |
| xdotool | insert keys to X |
| i3-gaps | window manager |
| i3blocks | blocks |
| i3lock | screen locker |
| i3status | status bar |
| sxhkd | keys |
| unclutter | no mouse |
| dunst | notification script |
| dmenu | generic menu for X |
| rofi | dmenu replacement |
| zathura | document viewer |
| zathura-pdf-mupdf | mupdf backend for zathura |
| zathura-djvu | djvu support for zathura |
| libreoffice | office suite |
| firefox | browser |
| gimp | image manipulation tool |

## CLI packages
| package | description         |
| ---     | ---                 |
| mpd     | music player daemon |
| ncmpcpp | mpd client |
| mpc | mpc client |
| youtube-dl | video downloader |
| feh | image viewer and wp setter |
| sxiv | image viewer |
| scrot | screenshoter |
| figlet | text magic tool |
| sdcv | stardict frontend |
| translate-shell | google translate client |
| aspell | spell checking |
| aspell-en | spell checking english |
| w3m | cli web browser |
| surfraw | search engine |
| newsboat | rss reader |
| neomutt | mail client |
| rtv | reddit client |
| calcurse | todo list and appointements |
| abook | address book |
| tree | list files |
| atool | archive management |
| rclone | backup utility |
| rsync | backup utility |
| pass | password manager |
| transmission-cli | torrent downloaded |
| mlocate | locate search |
| entr | file watcher |
| lsof | ps using file |
| docx2txt | docx to txt |
| ffmpegthumbnailer | thumbnails |
| odt2txt | odt to txt |
| imagemagick | image manipulation |

## Code packages
| package | description        |
| ---     | ---                |
| code    | visual studio code |
| zeal | documentation |
| jq | json parser |
| ripgrep | code search |
| bat | cat alternative |
| fzf | fuzzy finder |
| httpie | http client |
| hugo | SSG |
| yarn | node package manager |
| python-pip | python pip |
| virtualbox | hypervisor |
| virtualbox-host-modules-arch | hypervisor tools |
| vagrant | vm frontend |
| ansible | config management |
| podman | containers |

## Fonts packages
| package          | description |
| ---              | ---         |
| noto-fonts-emoji | noto font   |
| ttf-dejavu | dejavu font |
| ttf-droid | droid font |
| ttf-powerline | powerline |
| ttf-font-awesome | awesome |
| ttf-liberation | liberation |

## AUR Packages
| package          | description                |
| ---              | ---                        |
| yay              | mounting drives            |
| brave-bin        | browser                    |
| task-spooler     | task quee                  |
| urlview          | cli url viewer             |
| transmission-cli | curses transmission client |
| brave-bin        | browser                    |
| entr             | watch files                |
| btop             | Process Monitor            |

# Resources
- [Handling magnet urls with w3m](https://boeglin.org/blog/index.php?m=03&y=12&entry=entry120314-214917)
- [Mutt with Pass](http://schnizle.in/blog/posts/2014-11-24-Howto:-Setup-mutt-with-native-imap-+-msmpt-+-gpg-+-pass)
- [GNU/Linux Crypto](https://sanctum.geek.nz/arabesque/gnu-linux-crypto-introduction/)
- [gpg for humans](https://paul.fawkesley.com/gpg-for-humans-protecting-your-primary-key/)

