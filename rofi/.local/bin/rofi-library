#!/bin/zsh
DOCS="$HOME/Documents"
TERMINAL="alacritty"

books() {
    BOOKS_FOLDER="$DOCS/books"
    line=$(ls -p $BOOKS_FOLDER/*/* | grep -v /$ | grep -v :$ | rofi -dmenu -i -p Books)
    if [ -z $line ]; then
        exit 0
    else
        result=${line}
        linkhandler "$result"
    fi
}

docs() {
    DOCS_FOLDER="$DOCS/assets/docs"
    line=$(ls -p $DOCS_FOLDER | grep -v /$ | rofi -dmenu -i -p Books)
    if [ -z $line ]; then
        exit 0
    else
        result=${DOCS_FOLDER}/${line}
        linkhandler "$result"
    fi
}

dotfiles() {
    DOTFILES_FOLDER="$HOME/dotfiles"
    line=$(find $DOTFILES_FOLDER -type f | grep -v ".git" | rofi -dmenu -columns 1 -i -p Dotfiles)
    if [ -z $line ]; then
        exit 0
    else
        result=${line}
        st -c Library -e nvim "$result"
    fi
}

articles() {
    ARTICLES_FOLDER="$HOME/Work/blog/content/"
    line=$(find $ARTICLES_FOLDER -type f -regex ".*\.md" | rofi -dmenu -columns 1 -i -p Articles)
    if [ -z $line ]; then
        exit 0
    else
        result=${line}
        st -c Library -e nvim "$result"
    fi
}

search_engine() {
    BROWSER=firefox
    list="$(ls -1 $XDG_CONFIG_HOME/surfraw/elvi)"
    surfraw -browser=$BROWSER $(echo $list | rofi -dmenu -i -p "rofi-surfraw-websearch: ")
}

LIBRARY_FILE="$HOME/.local/share/rofi/library.txt"
LIBRARY_FOLDER="$DOCS/notes"

if ps -aux | grep "[L]ibrary" > /dev/null; then
    # i3-msg 'scratchpad show'
    id=$(xdotool search --class Library)

    if [ "$id" != "" ]
     then
      bspc node "$id" --flag hidden -f
    fi
else
    line=$(cat $LIBRARY_FILE | rofi -dmenu -i -p Library)
    result=$(echo "$line" | cut -f2 -d'-')

    if [ $result = "todo" ]; then
        $TERMINAL --class Library -e calcurse & disown
    elif [ $result = "dotfiles" ]; then
        dotfiles
    elif [ $result = "calc" ]; then
        $TERMINAL --class Library -e python -ic "from math import *; import cmath" & disown
    elif [ $result = "arch" ]; then
        $TERMINAL --class Library -e $FILE "/usr/share/doc/arch-wiki/html/en" & disown
    elif [ $result = "temp" ]; then
        $TERMINAL --class Library -e nvim "/tmp/thinkering.md" & disown
    elif [ $result = "books" ]; then
        books
    elif [ $result = "se" ]; then
        search_engine
    elif [ $result = "pass" ]; then
        passmenu
    elif [ -z $result ]; then
        exit 0
    else
        result="${LIBRARY_FOLDER}/${result}"
        st -c Library -e nvim $result & disown
    fi
fi
