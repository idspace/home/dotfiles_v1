---@type MappingsTable
local M = {}

M.general = {
  n = {
    [";"] = { ":", "enter command mode", opts = { nowait = true } },

    --  format with conform
    ["<leader>fm"] = {
      function()
        require("conform").format()
      end,
      "formatting",
    },

    -- Changed motions
    ["J"] = { "mzJ`z" },
    ["<C-d>"] = { "<C-d>zz" },
    ["<C-u>"] = { "<C-u>zz" },
    ["n"] = { "nzzzv" },
    ["N"] = { "Nzzzv" },
    ["<leader>d"] = { '[["_d]]'},

    -- Splits
    ["<leader>-"] = { "<cmd> split <CR>" },
    ["<leader>|"] = { "<cmd> vsplit <CR>" },

    -- commands
    ["<C-p>"] = { "<cmd> Telescope git_files <CR>", "Telescope git" },
    ["<leader><leader>"] = {
      function ()
        vim.cmd("so")
      end
    },
  },

  v = {
    [">"] = { ">gv", "indent"},
    ["J"] = { ":m '>+1<CR>gv=gv" },
    ["K"] = { ":m '<-2<CR>gv=gv" },
    ["<leader>d"] = { '[["_d]]'},
  },
}

-- more keybinds!

return M
